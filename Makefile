.PHONY: all
all:
	requirements install


.PHONY: clean
clean:
	rm -f *.egg-info


.PHONY: install
install:
	pip install -e .


.PHONY: uninstall
uninstall:
	pip uninstall RBM


.PHONY: requirements
requirements:
	pip install -r requirements.txt

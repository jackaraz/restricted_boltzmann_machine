import tensorflow as tf

from typing import Callable

from RBM.utils.sampler import sample_bernoulli

@tf.function
def contrastive_divergence_gibbs(
        x: tf.Tensor,
        w: tf.Tensor,
        vb: tf.Tensor,
        hb: tf.Tensor,
        dw: tf.Tensor,
        dvb: tf.Tensor,
        dhb: tf.Tensor,
        lr: float = 1e-2,
        momentum: float = 0.95,
        n_update: int = 1,
        sampler: Callable = sample_bernoulli,
):
    """

    Parameters
    ----------
    batch_data : tf.Tensor
        batched data (Nt, sites)
    weights : tf.Tensor
        trainable weights (visible_dim, hidden_dim)
    visible_biases : tf.Tensor
        (visible_dim)
    hidden_biases : tf.Tensor
        (hidden dim)
    n_update : int
        Number of Gibbs update

    Returns
    -------

    """
    def apply_momentum(old, new):
        N = tf.cast(new.shape[0], dtype=new.dtype)
        return tf.add(tf.scalar_mul(momentum,old), tf.scalar_mul((1. - momentum) * lr / N, new))

    h_prob_0 = tf.sigmoid(tf.matmul(x, w) + hb) # (Nt, hb)
    h_states_0 = sampler(h_prob_0)

    for _ in tf.range(0, n_update):
        v_prob_1   = tf.sigmoid(tf.matmul(h_states_0, tf.transpose(w)) + vb) # (Nt, vb)
        v_states_1 = sampler(v_prob_1)

        h_prob_1 = tf.sigmoid(tf.matmul(v_prob_1, w) + hb) # (Nt, hb)

        h_states_1 = sampler(h_prob_1)
        h_states_0 = h_states_1

    positive_grad = tf.matmul(tf.transpose(x), h_prob_0)
    negative_grad = tf.matmul(tf.transpose(v_prob_1), h_prob_1)

    dw.assign(apply_momentum(dw, positive_grad - negative_grad))
    dvb.assign(apply_momentum(dvb, tf.reduce_mean(x - v_prob_1, axis = 0)))
    dhb.assign(apply_momentum(dhb, tf.reduce_mean(h_prob_0 - h_prob_1, axis = 0)))

    w.assign_add(dw)
    vb.assign_add(dvb)
    hb.assign_add(dhb)

    return tf.reduce_mean(tf.losses.mean_squared_error(x, v_prob_1)), v_prob_1


@tf.function
def contrastive_divergence(
        x: tf.Tensor,
        w: tf.Tensor,
        vb: tf.Tensor,
        hb: tf.Tensor,
        dw: tf.Tensor,
        dvb: tf.Tensor,
        dhb: tf.Tensor,
        lr: float = 1e-2,
        momentum: float = 0.95,
):
    def apply_momentum(old, new):
        N = tf.cast(new.shape[0], dtype=new.dtype)
        return tf.add(tf.scalar_mul(momentum,old), tf.scalar_mul((1. - momentum) * lr / N, new))

    h_prob = tf.sigmoid(tf.matmul(x, w) + hb) # (Nt, hb)
    v_reco_prob = tf.sigmoid(tf.matmul(sample_bernoulli(h_prob), tf.transpose(w)) + vb) # (Nt, vb)
    h_reco_prob = tf.sigmoid(tf.matmul(v_reco_prob, w) + hb) # (Nt, hb)

    positive_grad = tf.matmul(tf.transpose(x), h_reco_prob)
    negative_grad = tf.matmul(tf.transpose(v_reco_prob), h_reco_prob)

    dw.assign(apply_momentum(dw, positive_grad - negative_grad))
    dvb.assign(apply_momentum(dvb, tf.reduce_mean(x - v_reco_prob, axis = 0)))
    dhb.assign(apply_momentum(dhb, tf.reduce_mean(h_prob - h_reco_prob, axis = 0)))

    w.assign_add(dw)
    vb.assign_add(dvb)
    hb.assign_add(dhb)

    return tf.reduce_mean(tf.losses.mean_squared_error(x, v_reco_prob)), v_reco_prob


@tf.function
def persistent_contrastive_divergence(
        x: tf.Tensor,
        w: tf.Tensor,
        vb: tf.Tensor,
        hb: tf.Tensor,
        dw: tf.Tensor,
        dvb: tf.Tensor,
        dhb: tf.Tensor,
        v_prob_init : tf.Tensor,
        h_prob_init : tf.Tensor,
        lr: float = 1e-2,
        momentum: float = 0.95,
):
    def apply_momentum(old, new):
        N = tf.cast(new.shape[0], dtype=new.dtype)
        return tf.add(tf.scalar_mul(momentum,old), tf.scalar_mul((1. - momentum) * lr / N, new))

    h_prob = tf.sigmoid(tf.matmul(x, w) + hb) # (Nt, hb)
    v_reco_prob = tf.sigmoid(tf.matmul(sample_bernoulli(h_prob), tf.transpose(w)) + vb) # (Nt, vb)
    h_reco_prob = tf.sigmoid(tf.matmul(v_reco_prob, w) + hb) # (Nt, hb)

    positive_grad = tf.matmul(tf.transpose(v_prob_init), h_prob_init)
    negative_grad = tf.matmul(tf.transpose(v_reco_prob), h_reco_prob)

    dw.assign(apply_momentum(dw, positive_grad - negative_grad))
    dvb.assign(apply_momentum(
        dvb, tf.reduce_mean(v_prob_init - v_reco_prob[:tf.shape(v_prob_init)[0]], axis = 0)
    ))
    dhb.assign(apply_momentum(
        dhb, tf.reduce_mean(h_prob_init - h_reco_prob[:tf.shape(h_prob_init)[0]], axis = 0)
    ))

    w.assign_add(dw)
    vb.assign_add(dvb)
    hb.assign_add(dhb)

    return tf.reduce_mean(tf.losses.mean_squared_error(x, v_reco_prob)), v_reco_prob


@tf.function
def persistent_contrastive_divergence_gibbs(
        x: tf.Tensor,
        w: tf.Tensor,
        vb: tf.Tensor,
        hb: tf.Tensor,
        dw: tf.Tensor,
        dvb: tf.Tensor,
        dhb: tf.Tensor,
        v_prob_init : tf.Tensor,
        h_prob_init : tf.Tensor,
        lr: float = 1e-2,
        momentum: float = 0.95,
        n_update: int = 1,
        sampler: Callable = sample_bernoulli,
):
    """

    Parameters
    ----------
    batch_data : tf.Tensor
        batched data (Nt, sites)
    weights : tf.Tensor
        trainable weights (visible_dim, hidden_dim)
    visible_biases : tf.Tensor
        (visible_dim)
    hidden_biases : tf.Tensor
        (hidden dim)
    n_update : int
        Number of Gibbs update

    Returns
    -------

    """
    def apply_momentum(old, new):
        N = tf.cast(new.shape[0], dtype=new.dtype)
        return tf.add(tf.scalar_mul(momentum,old), tf.scalar_mul((1. - momentum) * lr / N, new))

    h_prob_0 = tf.sigmoid(tf.matmul(x, w) + hb) # (Nt, hb)
    h_states_0 = sampler(h_prob_0)

    for _ in tf.range(0, n_update):
        v_prob_1   = tf.sigmoid(tf.matmul(h_states_0, tf.transpose(w)) + vb) # (Nt, vb)
        v_states_1 = sampler(v_prob_1)

        h_prob_1 = tf.sigmoid(tf.matmul(v_prob_1, w) + hb) # (Nt, hb)

        h_states_1 = sampler(h_prob_1)
        h_states_0 = h_states_1

    positive_grad = tf.matmul(tf.transpose(v_prob_init), h_prob_init)
    negative_grad = tf.matmul(tf.transpose(v_reco_prob), h_reco_prob)

    dw.assign(apply_momentum(dw, positive_grad - negative_grad))
    dvb.assign(apply_momentum(
        dvb, tf.reduce_mean(v_prob_init - v_reco_prob[:tf.shape(v_prob_init)[0]], axis = 0)
    ))
    dhb.assign(apply_momentum(
        dhb, tf.reduce_mean(h_prob_init - h_reco_prob[:tf.shape(h_prob_init)[0]], axis = 0)
    ))

    w.assign_add(dw)
    vb.assign_add(dvb)
    hb.assign_add(dhb)

    return tf.reduce_mean(tf.losses.mean_squared_error(x, v_prob_1)), v_prob_1
import tensorflow as tf

from typing import Optional, Callable


def reconstruction_cross_entropy(
        truth: tf.Tensor, probabilities: tf.Tensor,
        reduce: Optional[Callable] = tf.reduce_mean
):
    return reduce(truth * tf.math.log(probabilities) + (1. - truth) * tf.math.log(1. - probabilities))


def reconstruction_binary_cross_entropy(
        truth: tf.Tensor, probabilities: tf.Tensor,
        reduce: Optional[Callable] = tf.reduce_mean,
):
    return reduce(tf.losses.binary_crossentropy(truth, probabilities))


def squared_error(
        samples: tf.Tensor, probabilities: tf.Tensor,
        reduce: Optional[Callable] = tf.reduce_mean,
):
    return reduce(tf.square(samples - probabilities))


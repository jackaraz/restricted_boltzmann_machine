from typing import List, Any, Union, Tuple, Text, Dict, Optional, Callable
import sys


def progress(count, total, status='Complete',start_time=-1, bar_len=30, id="Progress"):
    bar_len = bar_len
    if total == 0: total = 1.
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '█' * filled_len + '-' * (bar_len - filled_len)
    if start_time > 0.:
        status += f" Elapsed: {get_elapsed_time(start_time)}"
    status = status.ljust(len(status)+5)

    sys.stdout.write(id+': |%s| \x1b[32m%s%s\x1b[0m %s\r' % \
                     (bar, percents, '%', status.ljust(50)))
    sys.stdout.flush()


def get_elapsed_time(start_time):
    elapsed_time = time.time() - start_time
    days = 0
    if elapsed_time >= 86400.:
        days = int(elapsed_time / 86400.)
    elapsed_time = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    if elapsed_time.split(":")[1] == "00":
        return (days>0)*(f"{days} day(s) ")+elapsed_time.split(":")[2]+" sec."
    elif elapsed_time.split(":")[0] == "00":
        return (days>0)*(f"{days} day(s) ")+":".join(elapsed_time.split(":")[1:])
    else:
        return (days>0)*(f"{days} day(s) ")+elapsed_time


class history:
    def __init__(self):
        self.history = dict()

    def __len__(self):
        return max([len(item) for k, item in self.history.items()])

    def __getitem__(self, item):
        return self.history.get(item, [-1])

    def __setitem__(self, key, value):
        if key not in self.history.keys():
            self.history[key] = []
        self.history[key].append(value)

    def to_csv(self, filename: Text):
        filename += ".gz" if not any([x in filename for x in [".gz",".tgz",".tar.gz"]]) else ""
        import pandas as pd
        df = pd.DataFrame(self.history)
        df.to_csv(filename, float_format='%.5e', compression="gzip", index=False)

    @property
    def nepoch(self):
        return len(self)

    def items(self):
        return self.history.items()

    def val_items(self):
        return (
            (k,i) for k,i in self.history.items()
            if any([x in k for x in ["val", "validation", "test"]])
        )

    def train_items(self):
        return ((k,i) for k,i in self.history.items() if "train" in k)
import tensorflow as tf

def sample_binomial(probabilities):
    return tf.random.stateless_binomial(
        shape = tf.shape(probabilities),
        seed = tf.random.uniform(shape=(2,), minval=0, maxval=1000, dtype=tf.int32),
        counts = 1, probs = probabilities,
        output_dtype=probabilities.dtype,
    )

def sample_bernoulli(probabilities):
    return tf.nn.relu(tf.sign(probabilities - tf.random.uniform(tf.shape(probabilities))))


def sample_gaussian(probabilities):
    return probabilities + tf.random.uniform(tf.shape(probabilities), dtype=probabilities.dtype)

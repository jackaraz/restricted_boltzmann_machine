import tensorflow as tf
tf.get_logger().setLevel("ERROR")

from typing import List, Any, Union, Tuple, Text, Dict, Optional, Callable

from RBM.utils.training import (
    contrastive_divergence_gibbs, contrastive_divergence,
    persistent_contrastive_divergence, persistent_contrastive_divergence_gibbs
)
from RBM.utils.sampler import sample_bernoulli, sample_binomial, sample_gaussian
from RBM.utils.utils import progress, history

from functools import partial
import datetime
import os, json


class RBM:
    """
    Restricted Boltzmann Machine


    Parameters
    ----------
    visible_dim : int
        visible dimensions of RBM. data dimensions should be (Nt, visible_dim)
    hidden_dim : int
        hidden dimensions of the network
    initializer : Text, default glorot_uniform
        random number initializer
    dtype : tf.DType, default tf.float32
        datatype of the network
    training_algorithm : Text, default contrastive divergence
        The algorithm to train the network
    """
    algos = ["contrastive_divergence",
            "contrastive_divergence_gibbs",
            "persistent_contrastive_divergence",
            "persistent_contrastive_divergence_gibbs"]

    def __init__(
            self,
            visible_dim: int = -1,
            hidden_dim: int = -1,
            initializer: Optional[Text] = "gaussian_mean_0.0_std_0.1",
            dtype: Optional[tf.DType] = tf.float32,
            training_algorithm: Optional[Text] = "contrastive_divergence",
            sampler: Optional[Text] = "bernoulli",
    ):
        self.weights, self.visible_biases, self.hidden_biases = [-1]*3
        self._dw, self._dvb, self._dhb = [-1]*3
        self.dtype = dtype

        self._model_params = {
            "sampler" : sampler,
            "lr" : 1e-2,
            "momentum" : 0.95,
            "ngibbs" : -1,
            "training_algorithm" : training_algorithm,
            "visible_dims" : visible_dim,
            "hidden_dims" : hidden_dim,
            "initializer" : initializer,
            "persistent_size" : -1,
            "persistent_update_rate" : -1,
        }


    @classmethod
    def load_from_json(cls, dictionary: Union[Text, Dict]):
        if os.path.isfile(dictionary):
            with open(dictionary, "r") as f:
                dictionary = json.load(f)
        elif not isinstance(dictionary, dict):
            raise ValueError("Unknown input.")

        rbm = cls(-1,-1)
        for key in rbm._model_params.keys():
            if key in dictionary.keys():
                rbm._model_params[key] = dictionary[key]
            else:
                raise ValueError(f"Input dictionary does not have {key} property.")
        return rbm


    def save_model(self, filename: Text) -> None:
        """
        Save model parameters

        Parameters
        ----------
        filename : Text
        """
        with open(filename, "w") as f:
            json.dump(self._model_params, f, indent=4, sort_keys=True)


    def save_weights(self, filename: Text) -> None:
        """
        Save weights to a file

        Parameters
        ----------
        filename : Text
            file path
        """
        assert filename.endswith(".h5") or filename.endswith(".hdf5"), \
            "Only HDF5 files are supported"
        import h5py
        hf = h5py.File(filename, "w")
        rbm = hf.create_group("rbm")
        rbm.create_dataset("weights", data = self.weights, compression = "gzip")
        rbm.create_dataset("dw", data = self._dw, compression = "gzip")
        rbm.create_dataset("visible_biases", data = self.visible_biases, compression = "gzip")
        rbm.create_dataset("dvb", data = self._dvb, compression = "gzip")
        rbm.create_dataset("hidden_biases", data = self.hidden_biases, compression = "gzip")
        rbm.create_dataset("dhb", data = self._dhb, compression = "gzip")
        hf.close()

    @property
    def dtype(self) -> tf.DType:
        return self._dtype

    @dtype.setter
    def dtype(self, dtype: tf.DType) -> None:
        if isinstance(dtype, tf.DType):
            self.weights = tf.cast(self.weights, dtype = dtype)
            self.visible_biases = tf.cast(self.visible_biases, dtype = dtype)
            self.hidden_biases = tf.cast(self.hidden_biases, dtype = dtype)
            self._dw = tf.cast(self._dw, dtype = dtype)
            self._dvb = tf.cast(self._dvb, dtype = dtype)
            self._dhb = tf.cast(self._dhb, dtype = dtype)
            self._dtype = dtype
        else:
            raise ValueError("DType needs to be `tf.DType`.")

    def load_weights(self, filename: Text) -> None:
        """
        Load weights from a file
        Parameters
        ----------
        filename : Text
            file path
        """
        import h5py
        hf = h5py.File(filename,"r")
        rbm = hf.get("rbm")
        self.weights = tf.convert_to_tensor(rbm.get("weights"), dtype = self.dtype)
        self._dw = tf.convert_to_tensor(rbm.get("dw"), dtype = self.dtype)
        self.visible_biases = tf.convert_to_tensor(rbm.get("visible_biases"), dtype = self.dtype)
        self._dvb = tf.convert_to_tensor(rbm.get("dvb"), dtype = self.dtype)
        self.hidden_biases = tf.convert_to_tensor(rbm.get("hidden_biases"), dtype = self.dtype)
        self._dhb = tf.convert_to_tensor(rbm.get("dhb"), dtype = self.dtype)
        hf.close()


    def __repr__(self):
        return "\n".join([
            f"{key} ".ljust(25, " ") + f": {item}" for key, item in self._model_params.items()
        ])

    @property
    def learning_rate(self) -> float:
        """
        Learning rate for the training algorithm
        """
        return self._model_params["lr"]


    @learning_rate.setter
    def learning_rate(self, val: float):
        self._model_params["lr"] = float(val)


    @property
    def momentum(self) -> float:
        """
        Momentum of the updating algorithm
        """
        return self._model_params["momentum"]


    @momentum.setter
    def momentum(self, val: float):
        self._model_params["momentum"] = float(val)


    @property
    def ngibbs(self) -> int:
        """
        Number of Gibbs sampling
        """
        return self._model_params["ngibbs"]


    @ngibbs.setter
    def ngibbs(self, val: int):
        self._model_params["ngibbs"] = int(val)


    @property
    def sampler(self) -> Callable:
        """
        Sampler method for Gibbs sampling
        """
        return {
            "bernoulli" : sample_bernoulli,
            "binomial" : sample_binomial,
            "gaussian" : sample_gaussian,
        }[self._model_params["sampler"]]


    @sampler.setter
    def sampler(self, val: Text):
        if val in ["bernoulli", "binomial", "gaussian"]:
            self._model_params["sampler"] = val
        else:
            raise ValueError(f"Unknown sampler : {val}")


    def IsPersistent(self) -> bool:
        """
        Is the network using a persistent algorithm for training
        """
        return ("persistent" in self._model_params["training_algorithm"])

    @property
    def persistent_size(self) -> int:
        """
        Size of the training dataset to be used in persistent algorithm
        """
        return self._model_params["persistent_size"]

    @persistent_size.setter
    def persistent_size(self, val : int):
        if not self.IsPersistent():
            return ValueError(
                f"Training algorithm is not persistent. "
                f"Current algorithm is {self._model_params['training_algorithm']}"
            )
        else:
            self._model_params["persistent_size"] = int(val)

    @property
    def persistent_update_rate(self) -> int:
        """
        How many times per epoch should persistent data be updated
        """
        return self._model_params["persistent_update_rate"]


    @persistent_update_rate.setter
    def persistent_update_rate(self, val: int):
        if not self.IsPersistent():
            return ValueError(
                f"Training algorithm is not persistent. "
                f"Current algorithm is {self._model_params['training_algorithm']}"
            )
        else:
            self._model_params["persistent_update_rate"] = int(val)


    @property
    def training_algorithm(self) -> Callable:
        """
        Training algorithm to be used
        """
        algo = {
            "contrastive_divergence" : contrastive_divergence,
            "contrastive_divergence_gibbs" : contrastive_divergence_gibbs,
            "persistent_contrastive_divergence" : persistent_contrastive_divergence,
            "persistent_contrastive_divergence_gibbs" : persistent_contrastive_divergence_gibbs,
        }[self._model_params["training_algorithm"]]

        if "gibbs" in self._model_params["training_algorithm"]:
            return partial(
                algo, lr = self.learning_rate, momentum = self.momentum,
                n_update = self.ngibbs, sampler = self.sampler,
            )
        else:
            return partial(algo, lr = self.learning_rate, momentum = self.momentum)


    @training_algorithm.setter
    def training_algorithm(self, val: Text):
        if val in self.algos:
            self._model_params["training_algorithm"] = val
            self.ngibbs = max(self.ngibbs, 1) if "gibbs" in val else -1
            self.persistent_size = max(self.persistent_size, 2) if "persistent" in val else -1
            self.persistent_update_rate = \
                max(10, self.persistent_update_rate) if "persistent" in val else -1

        else:
            raise ValueError(f"Unknown training algorithm: {val}\n "
                             f"Available algorithms are " + ', '.join(self.algos))


    def build(self) -> None:
        """
        Build the network
        """
        if self._model_params["initializer"].startswith("gaussian"):
            if len(self._model_params["initializer"].split("_")) == 5:
                prm = self._model_params["initializer"].split("_")
                def init(shape = (), dtype = self.dtype):
                    try:
                        return tf.random.normal(
                            shape, mean=float(prm[2]), stddev=float(prm[4]), dtype = self.dtype
                        )
                    except ValueError as err:
                        raise ValueError("Syntax for gaussian initializer is"
                                         " `gaussian_mean_<float>_std_<float>`")
            else:
                raise ValueError("Syntax for gaussian initializer is "
                                 "`gaussian_mean_<float>_std_<float>`")
        else:
            init = tf.initializers.get(self._model_params["initializer"])
        self.weights = tf.Variable(
            init((self._model_params["visible_dims"], self._model_params["hidden_dims"]),
                 dtype = self.dtype), name = "weights"
        )
        self._dw = tf.Variable(tf.zeros(self.weights.shape, dtype = self.dtype))
        self.visible_biases = tf.Variable(
            init((self._model_params["visible_dims"],), dtype = self.dtype),
            name = "visible_biases"
        )
        self._dvb = tf.Variable(tf.zeros(self.visible_biases.shape, dtype = self.dtype))
        self.hidden_biases = tf.Variable(
            init((self._model_params["hidden_dims"],), dtype = self.dtype),
            name = "hidden_biases"
        )
        self._dhb = tf.Variable(tf.zeros(self.hidden_biases.shape, dtype = self.dtype))


    @property
    def trainable_variables(self) -> List[tf.Tensor]:
        """
        Trainable variables in the network
        Returns
        -------
        List[tf.Tensor]
            [weights, visible biases, hidden biases]
        """
        return [self.weights, self.visible_biases, self.hidden_biases]


    @property
    def derivatives(self) -> List[tf.Tensor]:
        return [self._dw, self._dvb, self._dhb]


    def hidden(self, sample: tf.Tensor) -> tf.Tensor:
        """
        Reconstruct hidden state probabilities
        Parameters
        ----------
        sample : tf.Tensor
            shape (Nt, visible dim)

        Returns
        -------
        tf.Tensor
        probability distribution with shape (Nt, hidden dims)
        """
        return tf.sigmoid(tf.matmul(sample, self.weights) + self.hidden_biases)


    def reconstruct(self, sample: tf.Tensor) -> tf.Tensor:
        """
        Reconstruct the sample
        Parameters
        ----------
        sample : tf.Tensor
            shape (Nt, visible dim)

        Returns
        -------
        tf.Tensor
            probability distribution with shape (Nt, visible dims)
        """
        if self.ngibbs > 0:
            return self.reconstruct_gibbs(sample)

        return tf.sigmoid(
            tf.matmul(self.hidden(sample), tf.transpose(self.weights)) + self.visible_biases
        )


    def reconstruct_gibbs(self, sample: tf.Tensor) -> tf.Tensor:
        """
        Reconstruct the sample via Gibbs sampling method

        Parameters
        ----------
        sample : tf.Tensor
            shape (Nt, visible dim)

        Returns
        -------
        tf.Tensor
            probability distribution with shape (Nt, visible dims)
        """
        assert self.ngibbs > 0, "Set number of Gibbs sampling."

        h_prob_0 = self.hidden(sample)
        h_states_0 = self.sampler(h_prob_0)

        for _ in range(self.ngibbs):
            v_prob_1   = tf.sigmoid(
                tf.matmul(h_states_0, tf.transpose(self.weights)) + self.visible_biases
            ) # (Nt, vb)
            v_states_1 = self.sampler(v_prob_1)

            h_prob_1 = tf.sigmoid(tf.matmul(v_prob_1, self.weights) + self.hidden_biases) # (Nt, hb)

            h_states_1 = self.sampler(h_prob_1)
            h_states_0 = h_states_1

        return v_prob_1


    def partition_function(self, sample: tf.Tensor) -> tf.Tensor:
        hidden_state  = self.sampler(self.hidden(sample))

        Z = tf.exp(tf.tensordot(hidden_state, self.hidden_biases, axes = (-1,0))) \
            * tf.reduce_prod(
            self.visible_biases + tf.einsum("vh,nh->nv", self.weights, hidden_state), axis=1
        )

        return tf.reduce_sum(Z)


    def energy(self, sample: tf.Tensor) -> tf.Tensor:
        """
        .. math::
            E_{\\rm RBM}(\\mathbf{x},\mathbf{h};W) = W_{ij}\\mathbf{x}_i\\mathbf{h}_j
                                                    + b_i\\mathbf{x}_i + c_i\\mathbf{h}_i

        x = visible state, h = hidden state, b = visible biases, c = hidden biases, W = weights

        Parameters
        ----------
        sample : tf.Tensor

        Returns
        -------
        tf.Tensor
        """
        # visible_state = self.sampler(self.reconstruct(sample))
        hidden_state  = self.sampler(self.hidden(sample))

        return tf.vectorized_map(
            lambda states: tf.tensordot(states[0], self.visible_biases, axes=(0,0)) \
                           + tf.tensordot(states[1], self.hidden_biases, axes=(0,0)) \
                           + tf.einsum("v,vh,h",states[0],self.weights,states[1]),
            (sample, hidden_state)
        )


    def free_energy(self, sample: tf.Tensor) -> tf.Tensor:
        """
        .. math::
            F(v) = -\\log\\sum_h e^{-E(\\mathbf{v},\mathbf{h})} =
            - \\mathbf{b}\\mathbf{v} - \\sum_i \\log(1 + e^{c_i + W_i v}

        v = visible state, h = hidden state, b = visible biases, c = hidden biases, W = weights

        Parameters
        ----------
        sample : tf.Tensor

        Returns
        -------
        tf.Tensor
        """
        visible_state = self.sampler(self.reconstruct(sample))
        bv = tf.tensordot(visible_state, self.visible_biases, axes = (-1,0))
        wx_b = self.hidden_biases + tf.matmul(visible_state, self.weights)

        return - bv - tf.reduce_sum(tf.math.log(1. + tf.exp(wx_b)))


    def train(
            self, training_sample: tf.data.Dataset, test_sample: Optional[tf.Tensor] = None,
            nepoch: int = 1, batch_size: int = 128, val_batch_size: int = 128,
            training_log: Optional[tf.summary.SummaryWriter] = None,
            validation_log : Optional[tf.summary.SummaryWriter] = None,
    ) -> List[float]:
        assert isinstance(training_sample, tf.data.Dataset), \
            "Training sample has to be `tf.data.Dataset`"
        if self.IsPersistent():
            assert 2 < self.persistent_size <= batch_size, \
                "Size of persistent sample should be less than or equal to the batch size."
        if test_sample is not None:
            assert isinstance(test_sample, tf.data.Dataset), \
                "Test sample has to be `tf.data.Dataset`"

        try:
            hist = history(); init = True
            for epoch in range(1, int(nepoch) + 1):
                if epoch%self.persistent_update_rate == 0 or init and self.IsPersistent():
                    init = False
                    initial_visible = tf.cast(
                        tf.stack(list(
                            training_sample.take(self.persistent_size).as_numpy_iterator()
                        )), dtype = self.dtype
                    )
                    initial_hidden = self.hidden(initial_visible)
                batch_hist = history()
                for current_training_batch in training_sample.batch(int(batch_size)):
                    if self.IsPersistent():
                        loss, reco = self.training_algorithm(
                            current_training_batch,
                            *self.trainable_variables, *self.derivatives,
                            initial_visible, initial_hidden
                        )
                    else:
                        loss, reco = self.training_algorithm(
                            current_training_batch,
                            *self.trainable_variables,
                            *self.derivatives,
                        )
                    batch_hist["mse"] = loss
                    batch_hist["binary_xentropy"] = tf.reduce_mean(
                        tf.losses.binary_crossentropy(current_training_batch, reco)
                    )
                    batch_hist["KLD"] = tf.reduce_mean(
                        tf.losses.kld(current_training_batch, reco)
                    )
                    batch_hist["exp_energy"] = tf.exp(-self.energy(current_training_batch))
                    batch_hist["exp_free_energy"] = tf.exp(self.free_energy(current_training_batch))

                    # log_likelihood = (
                    #         - tf.reduce_mean(self.free_energy(current_training_batch)) \
                    #         - tf.math.log(self.partition_function(current_training_batch))
                    # )
                    # batch_hist.append("LogLikelihood", log_likelihood)

                    progress(
                        len(batch_hist)*int(batch_size), training_sample.cardinality().numpy(),
                        status = (", ".join([f"{k} : {i[-1]:.5f}" for k,i in hist.train_items()]) \
                                  if epoch > 1 else "Running..."),
                        id = f"Epoch {epoch}/{nepoch} ", bar_len = 15,
                    )

                for key, item in batch_hist.items():
                    hist[f"train_{key}"] = tf.reduce_mean(tf.stack(item)).numpy()
                del batch_hist

                if isinstance(training_log, tf.summary.SummaryWriter):
                    with training_log.as_default():
                        for key, item in hist.train_items():
                            tf.summary.scalar(key.replace("train_", ""), item[-1], epoch)

                if isinstance(test_sample, tf.data.Dataset):
                    batch_hist = history()
                    for current_test_batch in test_sample.batch(int(val_batch_size)):
                        reco = self.reconstruct(current_test_batch)
                        mse = tf.reduce_mean(tf.losses.mean_squared_error(current_test_batch, reco))
                        batch_hist["mse"] = mse
                        batch_hist["exp_energy"] = tf.exp(-self.energy(current_test_batch))
                        batch_hist["exp_free_energy"] = tf.exp(self.free_energy(current_test_batch))
                        batch_hist["binary_xentropy"] = tf.reduce_mean(
                            tf.losses.binary_crossentropy(current_test_batch, reco)
                        )
                        batch_hist["KLD"] = tf.reduce_mean(tf.losses.kld(current_test_batch, reco))
                        # log_likelihood = (
                        #         - tf.reduce_mean(self.free_energy(current_test_batch)) \
                        #         - tf.math.log(self.partition_function(current_test_batch))
                        # )
                        # batch_hist.append("LogLikelihood", log_likelihood)

                    for key, item in batch_hist.items():
                        hist[f"val_{key}"] = tf.reduce_mean(tf.stack(item)).numpy()
                    del batch_hist
                    print("\n" + ", ".join([f"{k} : {i[-1]:.5f}" for k,i in hist.val_items()]))
                    if isinstance(validation_log, tf.summary.SummaryWriter):
                        with validation_log.as_default():
                            for key, item in hist.val_items():
                                tf.summary.scalar(key.replace("val_", ""), item[-1], epoch)

        except KeyboardInterrupt:
            print("   * Training stopped by the user.")
        except Exception as err:
            print("   * An exception has occured during training.")
            print(str(err))
            return hist

        return hist
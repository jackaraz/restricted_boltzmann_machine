#!/usr/bin/env python
# coding: utf-8

# # MNIST Dataset

# In[1]:
import os.path

import tensorflow as tf
import numpy as np
import RBM, datetime, os
import matplotlib.pyplot as plt


# In[2]:


binarized_dataset = False

(x_train, _), (x_test, _) = tf.keras.datasets.mnist.load_data()

x_train = x_train / 255
x_test = x_test / 255

if binarized_dataset:
    from sklearn.preprocessing import Binarizer
    binarizer = Binarizer()
    transformer = binarizer.fit(x_train.reshape(-1,28*28))
    x_train = binarizer.transform(x_train.reshape(-1,28*28))
    x_test = binarizer.transform(x_test.reshape(-1,28*28) )


# Transform `numpy.array` into `tf.data.Dataset`

# In[3]:


dataset = tf.data.Dataset.from_tensor_slices(x_train.reshape(-1,28*28).astype(np.float32))
dataset = dataset.shuffle(x_train.shape[0], reshuffle_each_iteration=True)

dataset_test = tf.data.Dataset.from_tensor_slices(x_test.reshape(-1,28*28).astype(np.float32))
dataset_test = dataset.shuffle(x_test.shape[0], reshuffle_each_iteration=True)


# In[7]:


rbm = RBM.RBM(
    visible_dim = 28*28,
    hidden_dim = 64
)
rbm.training_algorithm = "contrastive_divergence_gibbs"
rbm.ngibbs = 10

current_time = datetime.datetime.now().strftime("%b%d-%H_%M_%S")
train_summary_writer = tf.summary.create_file_writer("log/"+current_time+"/train/")
val_summary_writer = tf.summary.create_file_writer("log/"+current_time+"/validation/")

rbm.build()
hist = rbm.train(
    training_sample = dataset, test_sample=dataset_test, 
    nepoch = 100, batch_size = 10, val_batch_size = 100,
    training_log = train_summary_writer, validation_log = val_summary_writer,
)


# In[11]:


for ix, image in enumerate(x_test[:4]):
    image = image.reshape(1,28*28)
    reco = rbm.reconstruct(tf.cast(image, rbm.dtype))

    fig, axarr = plt.subplots(1, 2, sharey=True)
    fig.set_size_inches(12, 6)
    axarr[0].imshow(image[0].reshape(28,28))
    axarr[1].imshow(tf.reshape(reco,(28,28)).numpy())
    axarr[0].set_title('truth')
    axarr[1].set_title("reconstructed image")
    plt.tight_layout()

    plt.savefig(f"image_{ix}.png", bbox_inches = 'tight')


rbm.save_model(current_time+".json")
rbm.save_weights(current_time+".h5")
